/*
 * TimingManager.h
 *
 *  Created on: Feb 12, 2015
 *      Author: mdonze
 */

#ifndef TIMINGMANAGER_H_
#define TIMINGMANAGER_H_
#include <atomic>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include <DIMParamList.h>
#include <DIMXMLService.h>
#include <Queue.h>
#include <stdint.h>

#include "LTIM.h"
#include "TimingReader.h"
#include "TimingSimulator.h"

namespace ntof {
namespace timing {

/**
 * This class manages timing parameters and operating modes
 */
class TimingManager : public ntof::dim::DIMParamListHandler
{
public:
    enum Param
    {
        PARAM_MODE = 1,
        PARAM_TRIG_REPEAT = 2,
        PARAM_TRIG_PERIOD = 3,
        PARAM_TRIG_PAUSE = 4,
        PARAM_EVT_NUM = 5,
        PARAM_CALIB_OUT = 6,
        PARAM_EXTRA = 7
    };

    enum Mode
    {
        MODE_DISABLED = 0,
        MODE_AUTOMATIC = 1,
        MODE_CALIBRATION = 2
    };

    enum Output
    {
        OUT_DISABLED = 0,
        OUT_ENABLED = 1
    };

    TimingManager(std::string &cfgPath);
    virtual ~TimingManager();

    int parameterChanged(std::vector<ntof::dim::DIMData> &settingsChanged,
                         const ntof::dim::DIMParamList &list,
                         int &errCode,
                         std::string &errMsg);

    void run();

    /**
     * Called by timing reader when a timing event occured
     * @param trigTS Timestamp when trigger occured
     * @param cycleTS Cyclestamp when trigger occured
     * @param dest Beam destination
     * @param dest2 Beam destination 2
     * @param user Particle user
     */
    void ltimEventOccured(int64_t trigTS,
                          int64_t cycleTS,
                          const std::string &dest,
                          const std::string &dest2,
                          const std::string &user,
                          int32_t cycleNb);

private:
    ntof::dim::DIMParamList *params;
    TimingSimulator simulator;
    ntof::dim::DIMXMLService *aqnSvc;
    int64_t evtNumber; ///!<< Event number
    TimingReader reader;
    typedef std::shared_ptr<LTIM> ltimPtr;
    typedef std::vector<ltimPtr> ltimVec;
    ltimVec ltims;   //!<< Collection of LTIM devices
    ltimPtr simLTIM; //!<< LTIM for trigger simulation

    std::mutex mutex_; //!<< To synchronize data
    std::atomic<int> m_modeChange;
    ntof::utils::Queue<std::vector<ntof::dim::DIMData>> m_queue;

    /**
     * Change timing operating mode
     * @param newMode
     */
    void changeMode(int32_t newMode);
};

} /* namespace timing */
} /* namespace ntof */

#endif /* TIMINGMANAGER_H_ */
