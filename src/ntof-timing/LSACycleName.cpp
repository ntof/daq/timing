/*
 * LSACycleName.cpp
 *
 *  Created on: May 26, 2020
 *      Author: mdonze
 */

#include "LSACycleName.h"

#include <iostream>
#include <string>

#include <cmw-data/DataExceptions.h>

#include "RDA3Client.h"

namespace ntof {
namespace timing {

std::unique_ptr<LSACycleName> LSACycleName::s_instance;
std::string LSACycleName::s_fesaDevice = "CPS.TIMING.INFO";

LSACycleName::LSACycleName()
{
    try
    {
        m_subscription = RDA3Client::subscribe(
            s_fesaDevice, "LsaCyclesMapping",
            cmw::rda3::client::NotificationListenerSharedPtr(this));
    }
    catch (std::exception &ex)
    {
        std::cerr << "Unable to connect to the TimingInfo FESA class"
                  << std::endl;
        std::cout << "Error : " << ex.what() << std::endl;
    }
}

LSACycleName::~LSACycleName()
{
    if (m_subscription)
        m_subscription->unsubscribe();
}

void LSACycleName::setTimingInfoDevice(const std::string &deviceName)
{
    s_fesaDevice = deviceName;
}

bool LSACycleName::getLSACycleName(const std::string &user,
                                   std::string &lsaCycle,
                                   uint32_t timeout)
{
    if (!s_instance)
        s_instance.reset(new LSACycleName());
    return s_instance->tryGetLSACycleName(user, lsaCycle, timeout);
}

bool LSACycleName::tryGetLSACycleName(const std::string &user,
                                      std::string &lsaCycle,
                                      uint32_t timeout)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    LSAMap::const_iterator it = m_lsaNames.find(user);
    if (it != m_lsaNames.end())
    {
        lsaCycle = it->second;
        return true;
    }

    // Not immediatly found, wait
    if (m_cond_var.wait_for(
            lock, std::chrono::milliseconds(timeout), [this, user]() {
                return m_lsaNames.find(user) != m_lsaNames.end();
            }))
    {
        lsaCycle = m_lsaNames.find(user)->second;
        return true;
    }
    lsaCycle = "TIMEOUT";
    return false;
}

void LSACycleName::dataReceived(
    const cmw::rda3::client::Subscription &subscription,
    std::unique_ptr<cmw::rda3::common::AcquiredData> acqData,
    cmw::rda3::common::UpdateType updateType)
{
    try
    {
        const cmw::data::Data &d = acqData->getData();
        size_t usersSize;
        const char *const *users = d.getArrayString("users", usersSize);

        size_t cyclesSize;
        const char *const *cycles = d.getArrayString("cycles", cyclesSize);

        std::unique_lock<std::mutex> lock(m_mutex);
        for (size_t i = 0; i < usersSize; ++i)
            m_lsaNames[users[i]] = cycles[i];
        m_cond_var.notify_all();
    }
    catch (const cmw::data::DataException &ex)
    {
        std::cerr << "Data exception : " << ex.what() << std::endl;
    }
}

void LSACycleName::errorReceived(
    const cmw::rda3::client::Subscription &subscription,
    std::unique_ptr<cmw::rda3::common::RdaException> exception,
    cmw::rda3::common::UpdateType updateType)
{
    std::cerr << "RDA3 exception : " << exception->what() << std::endl;
}

} /* namespace timing */
} /* namespace ntof */
