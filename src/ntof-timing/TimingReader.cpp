/*
 * TimingReader.cpp
 *
 *  Created on: May 12, 2015
 *      Author: mdonze
 */

#include "TimingReader.h"

#include <iostream>

#include <timdt-lib-cpp/Timing.h>

#include "TimingManager.h"

namespace ntof {
namespace timing {

/**
 * Create the reader and start it
 * @param eventName
 */
TimingReader::TimingReader(TimingManager &mgr) : mgr_(mgr) {}

TimingReader::~TimingReader() {}

/**
 * Start reading timing events
 */
void TimingReader::start(const std::string eventName)
{
    threads_.emplace_back(&TimingReader::eventGetter, this, eventName);
}

void TimingReader::eventGetter(const std::string &eventName)
{
    while (true)
    {
        try
        {
            Timing::ConnectionContext hConnectionContext;
            // Create connection to the CTR
            hConnectionContext.setQueue(true);
            hConnectionContext.setTimeOut(0);
            std::cout << "Connecting to event " << eventName << std::endl;
            // connect to the LTIM-HARD (this LTIM has to be declared ! )
            Timing::EventDescriptor eventDesc(eventName);
            hConnectionContext.connect(&eventDesc);
            while (true)
            {
                Timing::EventValue eventValue;
                Timing::Value value;
                Timing::Value value2;
                Timing::Value value3;
                Timing::Value value4;
                try
                {
                    hConnectionContext.wait(&eventValue);
                    eventValue.getFieldValue("PROG_DEST", &value);
                    eventValue.getFieldValue("PROG_DEST2", &value2);
                    eventValue.getFieldValue("USER", &value3);
                    eventValue.getFieldValue("BASIC_PERIOD_NB", &value4);
                }
                catch (Timing::Exception &e)
                {
                    std::cout << "Timing exception: " << e.what() << std::endl;
                    break;
                }
                int64_t trigTime = 0;
                int64_t cycleTime = 0;
                std::string dest;
                std::string dest2;
                std::string user;
                int cycleNb = 0;

                try
                {
                    tTimingTime hTimeHw = eventValue.getHwTimestamp();
                    trigTime = hTimeHw.time.tv_sec * 1000000000LL;
                    trigTime += hTimeHw.time.tv_nsec;
                }
                catch (Timing::Exception &e)
                {
                    std::cout << "Timing exception when getting HW timestamp: "
                              << e.what() << std::endl;
                }
                try
                {
                    tTimingTime hTimeCycle = eventValue.getCycleTimestamp();
                    cycleTime = hTimeCycle.time.tv_sec * 1000000000LL;
                    cycleTime += hTimeCycle.time.tv_nsec;
                }
                catch (Timing::Exception &e)
                {
                    std::cout << "Timing exception: " << e.what() << std::endl;
                }

                try
                {
                    dest = value.getAsString();
                    dest2 = value2.getAsString();
                    user = value3.getAsString();
                    cycleNb = value4.getAsUshort();
                }
                catch (Timing::Exception &e)
                {
                    std::cout << "Timing exception: " << e.what() << std::endl;
                }

                // Notify that we got a timing event
                mgr_.ltimEventOccured(trigTime, cycleTime, dest, dest2, user,
                                      cycleNb);
            }
        }
        catch (Timing::Exception &e)
        {
            std::cerr << "FATAL Timing exception: " << e.what() << std::endl;
            return;
        }
        catch (const std::exception &ex)
        {
            std::cerr << "STD exception : " << ex.what() << std::endl;
        }
    }
}

} /* namespace timing */
} /* namespace ntof */
