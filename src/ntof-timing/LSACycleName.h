/*
 * LSACycleName.h
 *
 *  Created on: May 26, 2020
 *      Author: mdonze
 */

#ifndef SRC_NTOF_TIMING_LSACYCLENAME_H_
#define SRC_NTOF_TIMING_LSACYCLENAME_H_

#include <condition_variable>
#include <map>
#include <mutex>

#include <cmw-rda3/client/service/ClientService.h>

namespace ntof {
namespace timing {

class LSACycleName : public cmw::rda3::client::NotificationListener
{
public:
    /**
     * Gets the LSA cycle name for the current user
     * @param user User to retrieve the cycle name
     * @param lsaCycle Reference to put the LSA cycle name
     * @param timeout Timeout in ms To wait if not available in cache
     * @return true if cycle retrived successfully
     */
    static bool getLSACycleName(const std::string &user,
                                std::string &lsaCycle,
                                uint32_t timeout = 1000);

    /**
     * Sets the TimingInfo fesa device name to be used.
     * This method needs to be called before calling getLSACycleName
     * @param deviceName Name of the FESA device
     */
    static void setTimingInfoDevice(const std::string &deviceName);

    void dataReceived(const cmw::rda3::client::Subscription &subscription,
                      std::unique_ptr<cmw::rda3::common::AcquiredData> acqData,
                      cmw::rda3::common::UpdateType updateType) override;

    void errorReceived(const cmw::rda3::client::Subscription &subscription,
                       std::unique_ptr<cmw::rda3::common::RdaException> exception,
                       cmw::rda3::common::UpdateType updateType) override;

    virtual ~LSACycleName();

private:
    LSACycleName();
    bool tryGetLSACycleName(const std::string &user,
                            std::string &lsaCycle,
                            uint32_t timeout = 1000);
    static std::unique_ptr<LSACycleName> s_instance;
    static std::string s_fesaDevice;

    typedef std::map<std::string, std::string> LSAMap;
    LSAMap m_lsaNames;
    cmw::rda3::client::SubscriptionSharedPtr m_subscription;
    std::mutex m_mutex;
    std::condition_variable m_cond_var;
    std::string m_requestedUser;
};

} /* namespace timing */
} /* namespace ntof */

#endif /* SRC_NTOF_TIMING_LSACYCLENAME_H_ */
