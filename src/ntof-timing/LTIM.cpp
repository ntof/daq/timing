/*
 * LTIM.cpp
 *
 *  Created on: Feb 13, 2015
 *      Author: mdonze
 */

#include "LTIM.h"

#include <iostream>

#include <timdt-lib-cpp/Timing.h>

#include "RDA3Client.h"
#include "TimingManager.h"
#include "TimingReader.h"
#include "TimingSimulator.h"

namespace ntof {
namespace timing {

LTIM::LTIM(std::string ltimName,
           const std::string name,
           const std::string dest,
           const std::string dest2,
           const std::string user) :
    ltimName_(ltimName),
    name_(name),
    dest_(dest),
    dest2_(dest2),
    user_(user),
    outputEnabled(false)
{
    // Get actual status
    try
    {
        std::unique_ptr<cmw::rda3::common::AcquiredData> acqData =
            RDA3Client::getProperty(ltimName, "OutEnable");
        if (acqData.get())
        {
            const cmw::data::Data &data = acqData->getData();
            outputEnabled = data.getBool("outEnabled");
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << "Unable to get output status on " << ltimName << std::endl;
    }
}

LTIM::~LTIM() {}

void LTIM::setOutputEnabled(bool enabled)
{
    outputEnabled = enabled;
    std::unique_ptr<cmw::data::Data> data = cmw::data::DataFactory::createData();
    data->append("outEnabled", enabled);
    RDA3Client::writeProperty(ltimName_, "OutEnable", std::move(data));
}

void LTIM::trig()
{
    std::unique_ptr<cmw::data::Data> data = cmw::data::DataFactory::createData();
    data->append("trig", true);
    RDA3Client::writeProperty(ltimName_, "Trig", std::move(data));
}

const std::string &LTIM::getLTIMName()
{
    return ltimName_;
}

const std::string &LTIM::getName()
{
    return name_;
}

bool LTIM::isOutputEnabled()
{
    return outputEnabled;
}

/**
 * Gets if this LTIM matches event
 * @param dest
 * @param dest2
 * @param user
 * @return
 */
bool LTIM::matchEvent(const std::string dest,
                      const std::string dest2,
                      const std::string user)
{
    if ((!dest_.empty()) && (dest_ != dest))
    {
        return false;
    }

    if ((!dest2_.empty()) && (dest2_ != dest2))
    {
        return false;
    }

    if ((!user_.empty()) && (user_ != user))
    {
        return false;
    }

    return true;
}

} /* namespace timing */
} /* namespace ntof */
