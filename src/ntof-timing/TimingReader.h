/*
 * TimingReader.h
 *
 *  Created on: May 12, 2015
 *      Author: mdonze
 */

#ifndef SRC_NTOF_TIMING_TIMINGREADER_H_
#define SRC_NTOF_TIMING_TIMINGREADER_H_

#include <string>
#include <thread>
#include <vector>

namespace ntof {
namespace timing {
class TimingManager;

/**
 * Timing reader loop used to read timing events from timing cable
 */
class TimingReader
{
public:
    /**
     * Create the reader and start it
     * @param eventName
     */
    TimingReader(TimingManager &mgr);
    virtual ~TimingReader();

    /**
     * Start reading timing events
     */
    void start(const std::string eventName);

private:
    void eventGetter(const std::string &eventName); //!<< Thread function to get event

    TimingManager &mgr_; //!<< Reference to our manager object
    std::vector<std::thread> threads_; //!<< Threads to read timing
};

} /* namespace timing */
} /* namespace ntof */

#endif /* SRC_NTOF_TIMING_TIMINGREADER_H_ */
