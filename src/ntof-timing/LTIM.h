/*
 * LTIM.h
 *
 *  Created on: Feb 13, 2015
 *      Author: mdonze
 */

#ifndef LTIM_H_
#define LTIM_H_

#include <memory>
#include <string>
#include <thread>

namespace ntof {
namespace timing {
class TimingManager;
/**
 * Represents a LTIM object to be manipulated over RDA
 * and using timdt to get notification
 */
class LTIM
{
public:
    LTIM(std::string ltimName,
         const std::string name,
         const std::string dest,
         const std::string dest2,
         const std::string user);
    virtual ~LTIM();
    void setOutputEnabled(bool enabled);
    void trig();
    const std::string &getLTIMName();
    const std::string &getName();
    bool isOutputEnabled();

    /**
     * Gets if this LTIM matches event
     * @param dest
     * @param dest2
     * @param user
     * @return
     */
    bool matchEvent(const std::string dest,
                    const std::string dest2,
                    const std::string user);

private:
    std::string ltimName_;
    std::string name_;
    std::string dest_;
    std::string dest2_;
    std::string user_;
    bool outputEnabled;
};

} /* namespace timing */
} /* namespace ntof */

#endif /* LTIM_H_ */
