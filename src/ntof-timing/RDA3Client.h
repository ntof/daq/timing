/*
 * RDA3Client.h
 *
 *  Created on: May 26, 2020
 *      Author: mdonze
 */

#ifndef NTOF_TIMING_RDA3CLIENT_H_
#define NTOF_TIMING_RDA3CLIENT_H_

#include <string>

#include <cmw-fwk/client/rbac/RbacLoginService.h>
#include <cmw-rda3/client/service/ClientService.h>

namespace ntof {
namespace timing {

class RDA3Client
{
public:
    RDA3Client() = delete;
    virtual ~RDA3Client() = delete;
    static void writeProperty(const std::string &devName,
                              const std::string &propName,
                              std::unique_ptr<cmw::data::Data> data);

    static std::unique_ptr<cmw::rda3::common::AcquiredData> getProperty(
        const std::string &devName,
        const std::string &propName);

    static cmw::rda3::client::SubscriptionSharedPtr subscribe(
        const std::string &devName,
        const std::string &propName,
        cmw::rda3::client::NotificationListenerSharedPtr listener,
        const std::string &selector = "");

protected:
    static std::unique_ptr<cmw::rda3::client::ClientService> client; //!<< RDA
                                                                     //!< client
                                                                     //!< reference
    static std::unique_ptr<cmw::RbacLoginService> rbacService; //!<< RBAC login
                                                               //!< service
    static bool cmwStarted;

    static void startCMWServices();
    static void stopCMWServices();
};

} /* namespace timing */
} /* namespace ntof */

#endif /* SRC_NTOF_TIMING_RDA3CLIENT_H_ */
