/*
 * TimingManager.cpp
 *
 *  Created on: Feb 12, 2015
 *      Author: mdonze
 */

#include "TimingManager.h"

#include <iostream>

#include <pugixml.hpp>

#include "LSACycleName.h"

#include <dis.hxx>

#define DEFAULT_PATH "/dsc/local/data/ntofTiming.xml"

namespace ntof {
namespace timing {

TimingManager::TimingManager(std::string &cfgPath) :
    params(NULL),
    simulator(*this),
    aqnSvc(NULL),
    evtNumber(0),
    reader(*this),
    m_modeChange(0),
    m_queue("TimingManager", 2048)
{
    std::string configPath = DEFAULT_PATH;
    if (!cfgPath.empty())
    {
        configPath = cfgPath;
    }

    std::string prefix;
    // Load configuration file
    pugi::xml_document doc;
    pugi::xml_parse_result res = doc.load_file(configPath.c_str());
    if (res)
    {
        std::string paramSvcName;
        std::string aqnSvcName;
        pugi::xml_node root = doc.first_child();
        DimServer::setDnsNode(root.child("dimDNSNode")
                                  .attribute("name")
                                  .as_string("pcgw25.cern.ch"));
        prefix = root.child("svcPrefix").attribute("name").as_string("");
        std::cout << "Using prefix : " << prefix << std::endl;
        paramSvcName = prefix + "Timing";
        aqnSvcName = prefix + "Timing/event";
        params = new ntof::dim::DIMParamList(paramSvcName);
        aqnSvc = new ntof::dim::DIMXMLService(aqnSvcName);

        pugi::xml_node ltimsNode = root.child("ltims");
        for (pugi::xml_node ltim = ltimsNode.first_child(); ltim;
             ltim = ltim.next_sibling())
        {
            std::string ltimType = ltim.name();
            if (ltimType == "calibration")
            {
                if (simLTIM.get() == NULL)
                {
                    simLTIM = ltimPtr(new LTIM(ltim.attribute("device").value(),
                                               ltim.attribute("name").value(),
                                               "CALIB", "", ""));
                }
            }
            else
            {
                ltims.push_back(
                    ltimPtr(new LTIM(ltim.attribute("device").value(),
                                     ltim.attribute("name").value(),
                                     ltim.attribute("dest").value(),
                                     ltim.attribute("dest2").value(),
                                     ltim.attribute("user").value())));
            }
        }

        if (simLTIM.get() == NULL)
        {
            std::cerr << "FATAL : No calibration device declared..."
                      << std::endl;
            exit(1);
        }

        if (root.child("xtim").attribute("device"))
        {
            std::string timInfoDevice =
                root.child("timingInfo").attribute("device").as_string("");
            LSACycleName::setTimingInfoDevice(timInfoDevice);
        }

        pugi::xml_node eventsNode = root.child("events");
        for (pugi::xml_node event = eventsNode.first_child(); event;
             event = event.next_sibling())
        {
            std::string eventName = event.attribute("name").as_string("");
            if(!eventName.empty())
                reader.start(eventName);
        }
        
    }
    else
    {
        std::cerr << "FATAL : Unable to parse configuration file..."
                  << std::endl;
        exit(1);
    }

    // Initialize parameters
    params->setHandler(this);

    params->addParameter(PARAM_TRIG_REPEAT, "Calibration trigger repeat", "",
                         simulator.getTriggerRepeat(), dim::AddMode::CREATE,
                         false);
    params->addParameter(PARAM_TRIG_PERIOD, "Calibration trigger period", "ms",
                         simulator.getTriggerPeriod(), dim::AddMode::CREATE,
                         false);
    params->addParameter(PARAM_TRIG_PAUSE, "Calibration trigger pause", "ms",
                         simulator.getTriggerPause(), dim::AddMode::CREATE,
                         false);
    params->addParameter(PARAM_EVT_NUM, "Event number", "", evtNumber,
                         dim::AddMode::CREATE, false);

    ntof::dim::DIMEnum outEnum;
    outEnum.addItem(OUT_DISABLED, "DISABLED");
    outEnum.addItem(OUT_ENABLED, "ENABLED");
    outEnum.setValue(simLTIM->isOutputEnabled() ? 1 : 0);
    params->addParameter(PARAM_CALIB_OUT, simLTIM->getName() + " out enabled",
                         "", outEnum, dim::AddMode::CREATE, false);

    int i = PARAM_EXTRA;
    bool ltimEn = false;
    for (ltimVec::iterator it = ltims.begin(); it != ltims.end(); ++it)
    {
        outEnum.setValue((*it)->isOutputEnabled() ? 1 : 0);
        ltimEn |= (*it)->isOutputEnabled();
        params->addParameter(i, (*it)->getName() + " out enabled", "", outEnum,
                             dim::AddMode::CREATE, false);
        ++i;
    }

    // Compute actual mode
    ntof::dim::DIMEnum modeEnum;
    modeEnum.addItem(MODE_DISABLED, "DISABLED");
    modeEnum.addItem(MODE_AUTOMATIC, "AUTOMATIC");
    modeEnum.addItem(MODE_CALIBRATION, "CALIBRATION");
    if (!ltimEn && !simLTIM->isOutputEnabled())
    {
        modeEnum.setValue(0);
    }
    else if (simLTIM->isOutputEnabled())
    {
        // Mode calibration
        modeEnum.setValue(MODE_CALIBRATION);
        simulator.start();
    }
    else
    {
        modeEnum.setValue(MODE_AUTOMATIC);
    }

    params->addParameter(PARAM_MODE, "mode", "", modeEnum, dim::AddMode::CREATE,
                         false);

    params->updateList();
    std::string srvName = prefix + "ntof-timing";
    DimServer::start(srvName.c_str());
}

TimingManager::~TimingManager()
{
    if (aqnSvc != NULL)
    {
        delete aqnSvc;
    }

    if (params != NULL)
    {
        delete params;
    }
}

/**
 * Called when a setting changed
 * @param settingsChanged
 * @param list
 * @param errCode
 * @param errMsg
 * @return
 */
int TimingManager::parameterChanged(
    std::vector<ntof::dim::DIMData> &settingsChanged,
    const ntof::dim::DIMParamList &list,
    int &errCode,
    std::string &errMsg)
{
    bool modeChange = false;
    try
    {
        for (std::vector<ntof::dim::DIMData>::iterator it =
                 settingsChanged.begin();
             it != settingsChanged.end(); ++it)
        {
            if (it->getIndex() >= PARAM_EXTRA)
            {
                if (it->getIndex() - PARAM_EXTRA >= ltims.size())
                {
                    errCode = 2;
                    errMsg = "Invalid data index";
                    return 1;
                }
            }
            else
            {
                switch (it->getIndex())
                {
                case PARAM_MODE:
                {
                    int calibOut = 0;
                    int ltimsOut = 0;
                    switch (it->getEnumValue().getValue())
                    {
                    case MODE_CALIBRATION:
                        calibOut = 1;
                        ltimsOut = 0;
                        break;
                    case MODE_AUTOMATIC:
                        calibOut = 0;
                        ltimsOut = 1;
                        break;
                    case MODE_DISABLED:
                        calibOut = 0;
                        ltimsOut = 0;
                        break;
                    }
                    /*
                        reset default values, will be applied by parent
                        parameterChanged call
                     */
                    params->lockParameterAt(PARAM_CALIB_OUT)
                        ->getEnumValue()
                        .setValue(calibOut);

                    for (uint32_t i = PARAM_EXTRA;
                         i < PARAM_EXTRA + ltims.size(); ++i)
                    {
                        params->lockParameterAt(i)->getEnumValue().setValue(
                            ltimsOut);
                    }
                    modeChange = true;
                    break;
                }
                case PARAM_TRIG_PAUSE:
                case PARAM_EVT_NUM:
                case PARAM_CALIB_OUT: break;
                case PARAM_TRIG_REPEAT:
                    // Change calibration repeat
                    if (it->getIntValue() <= 0)
                    {
                        errCode = 2;
                        errMsg = "Trigger repeat cannot be less than 1!";
                        return 1;
                    }
                    break;
                case PARAM_TRIG_PERIOD:
                    // Change calibration period
                    if (it->getIntValue() < 500)
                    {
                        errCode = 2;
                        errMsg = "Trigger period cannot be less than 500ms!";
                        return 1;
                    }
                    break;
                }
            }
        }
    }
    catch (const std::exception &e)
    {
        errCode = 1;
        errMsg = e.what();
        return 1;
    }
    if (modeChange)
    {
        m_modeChange++;
        // shutdown all outputs right away
        simLTIM->setOutputEnabled(false);
        for (ltimVec::iterator it = ltims.begin(); it != ltims.end(); ++it)
        {
            (*it)->setOutputEnabled(false);
        }
    }
    m_queue.post(settingsChanged);
    return 0;
}

void TimingManager::run()
{
    int32_t newMode = -1;
    std::vector<ntof::dim::DIMData> settingsChanged(std::move(m_queue.pop()));
    try
    {
        for (std::vector<ntof::dim::DIMData>::iterator it =
                 settingsChanged.begin();
             it != settingsChanged.end(); ++it)
        {
            if (it->getIndex() >= PARAM_EXTRA)
            {
                // Changing something into ltims
                ltims[it->getIndex() - PARAM_EXTRA]->setOutputEnabled(
                    it->getEnumValue().getValue() != 0);
            }
            else
            {
                switch (it->getIndex())
                {
                case PARAM_MODE: newMode = it->getEnumValue().getValue(); break;
                case PARAM_TRIG_REPEAT:
                    // Change calibration repeat
                    simulator.setTriggerRepeat(it->getIntValue());
                    break;
                case PARAM_TRIG_PERIOD:
                    // Change calibration period
                    simulator.setTriggerPeriod(it->getIntValue());
                    break;
                case PARAM_TRIG_PAUSE:
                    // Change calibration pause
                    simulator.setTriggerPause(it->getIntValue());
                    break;
                case PARAM_EVT_NUM:
                    // Change event number
                    evtNumber = it->getLongValue();
                    break;
                case PARAM_CALIB_OUT:
                    // Disable/enable simulation output
                    simLTIM->setOutputEnabled(it->getEnumValue().getValue() !=
                                              0);
                    break;
                }
            }
        }

        if (newMode >= 0)
        {
            m_modeChange--;
            /* change mode once all settings have been set */
            changeMode(newMode);
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << "FATAL : " << e.what();
        exit(1);
    }
}

/**
 * Change timing operating mode
 * @param newMode
 */
void TimingManager::changeMode(int32_t newMode)
{
    switch (newMode)
    {
    case MODE_DISABLED:
        // Mode DISABLED
        simLTIM->setOutputEnabled(false);
        simulator.stop();

        for (ltimVec::iterator it = ltims.begin(); it != ltims.end(); ++it)
        {
            (*it)->setOutputEnabled(false);
        }

        break;
    case MODE_AUTOMATIC:
        // Mode auto
        simLTIM->setOutputEnabled(false);
        simulator.stop();

        for (ltimVec::iterator it = ltims.begin(); it != ltims.end(); ++it)
        {
            (*it)->setOutputEnabled(true);
        }
        break;
    case MODE_CALIBRATION:
        // Mode calibration
        simLTIM->setOutputEnabled(true);

        for (ltimVec::iterator it = ltims.begin(); it != ltims.end(); ++it)
        {
            (*it)->setOutputEnabled(false);
        }
        simulator.start();
        break;
    }
}

/**
 * Called by timing reader when a timing event occured
 * @param trigTS Timestamp when trigger occured
 * @param cycleTS Cyclestamp when trigger occured
 * @param dest Beam destination
 * @param dest2 Beam destination 2
 * @param user Particle user
 */
void TimingManager::ltimEventOccured(int64_t trigTS,
                                     int64_t cycleTS,
                                     const std::string &dest,
                                     const std::string &dest2,
                                     const std::string &user,
                                     int32_t cycleNb)
{
    if (m_modeChange.load() > 0)
    {
        std::cerr << "WARNING : ignoring event during mode change: "
                  << "dest:" << dest << " dest2:" << dest2 << " user:" << user
                  << std::endl;
        return;
    }

    std::lock_guard<std::mutex> lock(mutex_);
    ltimPtr ltim;
    if (simulator.isActive())
    {
        if (simLTIM->matchEvent(dest, dest2, user))
        {
            ltim = simLTIM;
            ltim->trig();
        }
    }
    else
    {
        for (ltimVec::iterator it = ltims.begin(); it != ltims.end(); ++it)
        {
            if ((*it)->matchEvent(dest, dest2, user))
            {
                ltim = (*it);
                break;
            }
        }
    }

    if (ltim.get() != NULL)
    {
        // std::cout << "Event occurred on #" << cycleNb << " " <<
        // ltim->getName() << " : " << user << "=>" << dest << "==>" << dest2 <<
        // " Trigger : " << trigTS << " Cycle : " << cycleTS << std::endl;
        pugi::xml_document doc;
        pugi::xml_node timingNode = doc.append_child("timing");
        pugi::xml_node eventNode = timingNode.append_child("event");
        eventNode.append_attribute("name").set_value(ltim->getName().c_str());
        eventNode.append_attribute("timestamp")
            .set_value(static_cast<long long>(trigTS));
        eventNode.append_attribute("cyclestamp")
            .set_value(static_cast<long long>(cycleTS));
        eventNode.append_attribute("periodNB").set_value(cycleNb);
        eventNode.append_attribute("dest").set_value(dest.c_str());
        eventNode.append_attribute("dest2").set_value(dest2.c_str());
        eventNode.append_attribute("user").set_value(user.c_str());
        ++evtNumber;
        eventNode.append_attribute("eventNumber")
            .set_value(static_cast<long long>(evtNumber));

        std::string lsaCycleName;
        if (ltim == simLTIM)
        {
            lsaCycleName = "CALIBRATION";
        }
        else
        {
            LSACycleName::getLSACycleName(user, lsaCycleName);
        }
        eventNode.append_attribute("lsaCycle").set_value(lsaCycleName.c_str());

        aqnSvc->setData(doc);

        params->setValue(PARAM_EVT_NUM, evtNumber);
    }

    /*pugi::xml_document doc;
    pugi::xml_node timingNode = doc.append_child("timing");
    pugi::xml_node eventNode = timingNode.append_child("event");
    eventNode.append_attribute("name").set_value(ltim->getName().c_str());
    eventNode.append_attribute("timestamp").set_value(static_cast<long
    long>(trigTS));
    eventNode.append_attribute("cyclestamp").set_value(static_cast<long
    long>(cycleTS)); aqnSvc->setData(doc);*/
}

} /* namespace timing */
} /* namespace ntof */
