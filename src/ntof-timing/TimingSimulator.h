/*
 * TimingSimulator.h
 *
 *  Created on: Feb 12, 2015
 *      Author: mdonze
 */

#ifndef TIMINGSIMULATOR_H_
#define TIMINGSIMULATOR_H_

#include <condition_variable>
#include <memory>
#include <mutex>
#include <thread>

#include <stdint.h>

namespace ntof {
namespace timing {
class LTIM;
class TimingManager;

/**
 * This class is used to simulate timing events for calibration mode
 */
class TimingSimulator
{
public:
    TimingSimulator(TimingManager &mgr);
    virtual ~TimingSimulator();
    void start();
    void stop();

    /**
     * Gets trigger period
     * @return The trigger period in ms
     */
    int32_t getTriggerPeriod();

    /**
     * Gets trigger repetition
     * @return The trigger repetition count
     */
    int32_t getTriggerRepeat();

    /**
     * Gets pause between each trigger bunch
     * @return
     */
    int32_t getTriggerPause();

    /**
     * Gets trigger period
     * @param value
     */
    void setTriggerPeriod(int32_t value);

    /**
     * Sets trigger repetitions
     * @param value
     */
    void setTriggerRepeat(int32_t value);

    /**
     * Sets pause between each trigger bunch
     * @param value
     */
    void setTriggerPause(int32_t value);

    /**
     * Gets if the simulator is running
     * @return
     */
    bool isActive();

private:
    /**
     * Simulator thread function
     */
    void simulator();

    TimingManager &mgr_;
    int32_t trigPeriod;
    int32_t trigCount;
    int32_t trigPause;
    bool running; //!<< Flag to know if thread is running

    std::condition_variable m_cond;
    std::mutex m_lock;
    std::thread th; //!<< Thread to simulate trigger scheme
};

} /* namespace timing */
} /* namespace ntof */

#endif /* TIMINGSIMULATOR_H_ */
