/*
 * RDA3Client.cpp
 *
 *  Created on: May 26, 2020
 *      Author: mdonze
 */

#include "RDA3Client.h"

namespace ntof {
namespace timing {

std::unique_ptr<cmw::rda3::client::ClientService> RDA3Client::client;
std::unique_ptr<cmw::RbacLoginService> RDA3Client::rbacService;
bool RDA3Client::cmwStarted = false;

void RDA3Client::writeProperty(const std::string &devName,
                               const std::string &propName,
                               std::unique_ptr<cmw::data::Data> data)
{
    startCMWServices();
    client->getAccessPoint(devName, propName).set(std::move(data));
}

std::unique_ptr<cmw::rda3::common::AcquiredData> RDA3Client::getProperty(
    const std::string &devName,
    const std::string &propName)
{
    startCMWServices();
    return client->getAccessPoint(devName, propName).get();
}

cmw::rda3::client::SubscriptionSharedPtr RDA3Client::subscribe(
    const std::string &devName,
    const std::string &propName,
    cmw::rda3::client::NotificationListenerSharedPtr listener,
    const std::string &selector)
{
    startCMWServices();
    if (selector.empty())
    {
        return client->getAccessPoint(devName, propName).subscribe(listener);
    }

    return client->getAccessPoint(devName, propName)
        .subscribe(cmw::rda3::common::RequestContextFactory::create(selector),
                   listener);
}

void RDA3Client::startCMWServices()
{
    if (!cmwStarted)
    {
        if (client.get() == NULL)
        {
            client = cmw::rda3::client::ClientService::create();
        }
        rbacService.reset(new cmw::RbacLoginService(*client));
        rbacService->setLoginPolicy(rbac::LOCATION);
        rbacService->setApplicationName("nTOF timing");
        rbacService->setAutoRefresh(true);
        rbacService->start();
        cmwStarted = true;
    }
}

void RDA3Client::stopCMWServices()
{
    if (cmwStarted)
    {
        rbacService->stop();
        rbacService.reset();
        cmwStarted = false;
    }
}

} /* namespace timing */
} /* namespace ntof */
