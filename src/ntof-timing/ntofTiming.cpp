#include <string>

#include "TimingManager.h"

int main(int argc, char *args[])
{
    std::string cfgPath;

    if (argc > 1)
    {
        cfgPath = args[1];
    }
    ntof::timing::TimingManager mgr(cfgPath);

    while (true)
    {
        mgr.run();
    }
    return 0;
}
