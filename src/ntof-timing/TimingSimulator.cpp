/*
 * TimingSimulator.cpp
 *
 *  Created on: Feb 12, 2015
 *      Author: mdonze
 */

#include "TimingSimulator.h"

#include <iostream>

#include <sys/time.h>

#include "LTIM.h"
#include "TimingManager.h"

const int64_t SECONDS_TO_NANOSECONDS_FACTOR = static_cast<int64_t>(1e9);
const int64_t MICROSECONDS_TO_NANOSECONDS_FACTOR = 1000;

namespace ntof {
namespace timing {

TimingSimulator::TimingSimulator(TimingManager &mgr) :
    mgr_(mgr),
    trigPeriod(2400),
    trigCount(1),
    trigPause(0),
    running(false)
{}

TimingSimulator::~TimingSimulator() {}

void TimingSimulator::start()
{
    std::unique_lock<std::mutex> lock(m_lock);
    if (!running)
    {
        running = true;
        th = std::thread(&TimingSimulator::simulator, this);
    }
}

void TimingSimulator::stop()
{
    std::unique_lock<std::mutex> lock(m_lock);
    if (running)
    {
        running = false;
        lock.unlock();
        m_cond.notify_all();
        th.join();
    }
}

/**
 * Gets trigger period
 * @return The trigger period in ms
 */
int32_t TimingSimulator::getTriggerPeriod()
{
    return trigPeriod;
}

/**
 * Gets trigger repetition
 * @return The trigger repetition count
 */
int32_t TimingSimulator::getTriggerRepeat()
{
    return trigCount;
}

/**
 * Gets pause between each trigger bunch
 * @return
 */
int32_t TimingSimulator::getTriggerPause()
{
    return trigPause;
}

/**
 * Gets trigger period
 * @param value
 */
void TimingSimulator::setTriggerPeriod(int32_t value)
{
    trigPeriod = value;
}

/**
 * Sets trigger repetitions
 * @param value
 */
void TimingSimulator::setTriggerRepeat(int32_t value)
{
    trigCount = value;
}

/**
 * Sets pause between each trigger bunch
 * @param value
 */
void TimingSimulator::setTriggerPause(int32_t value)
{
    trigPause = value;
}

/**
 * Gets if the simulator is running
 * @return
 */
bool TimingSimulator::isActive()
{
    return running;
}

/**
 * Thread loop
 */
void TimingSimulator::simulator()
{
    try
    {
        std::unique_lock<std::mutex> lock(m_lock);
        while (running)
        {
            for (int32_t i = 0; running && (i < trigCount); ++i)
            {
                try
                {
                    struct timeval now;
                    gettimeofday(&now, 0);
                    int64_t ts = (now.tv_sec * SECONDS_TO_NANOSECONDS_FACTOR +
                                  now.tv_usec *
                                      MICROSECONDS_TO_NANOSECONDS_FACTOR);
                    mgr_.ltimEventOccured(ts, ts, "CALIB", "", "", (i + 1));
                }
                catch (...)
                {
                    std::cout << "Unable to output calibration trigger using "
                                 "CTRI (CMW problems?)"
                              << std::endl;
                }
                m_cond.wait_for(lock, std::chrono::milliseconds(trigPeriod));
            }
            if (running)
            {
                m_cond.wait_for(lock, std::chrono::milliseconds(trigPause));
            }
        }
    }
    catch (...)
    {
        std::cerr << "Unable to output trigger on CTRI" << std::endl;
    }
}

} /* namespace timing */
} /* namespace ntof */
