#
# global makefile
#

CPU ?= L867
CMW_MAKEFILE_VERSION ?= 2.x.x
PARENT_MAKEFILE ?= /acc/local/share/cmw/cmw-makefile/$(CMW_MAKEFILE_VERSION)/Make.cern

PRODUCT=timing
PROJECT=ntof
BIN_NAME=ntofTiming

main: all

style:
	find src -type f | xargs -n1 /opt/cquery/bin/cquery-clang-format -i

include $(PARENT_MAKEFILE)
